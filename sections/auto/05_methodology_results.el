;; -*- lexical-binding: t; -*-

(TeX-add-style-hook
 "05_methodology_results"
 (lambda ()
   (LaTeX-add-labels
    "fig:m2n"
    "fig:mn1"))
 :latex)

