%!TEX root = main.tex
\section{Joux--Vitse Crossbred algorithm}
Essentially, this algorithm involves the specialisation of variables and then solving the remaining ones.
The advantage of this technique is that we can avoid solving the initial or the specialised system via, for example, the general version of $F_5$.

The main differentiating factor between this algorithm and BooleanSolve/FXL is that the manipulation of the Macaulay Matrix is done before specialising any variables.
This is an advantage since linear algebra in a Macaulay Matrix is the most costly step of the BooleanSolve algorithm since it is performed $2^{n-k}$ times \cite{Vitse:2017cb}.
The Crossbred algorithm attempts to limit the size of the Macaulay Matrix to speed up the computation of a polynomial system's Gr\"obner Basis.
Note that this algorithm assumes semi-regularity.
Let $F \in \mathbb{F}_q[X = x_1,\hdots,x_n]$ be the polynomial system of $m$ equations over $n$ variables which we want to solve via the Crossbred algorithm.
The algorithm accepts three parameters,\begin{enumerate}
    \item $D$, the degree of the Macaulay Matrix of $F$.
          $D \geq 2$ and must be at least the degree of regularity of a system with $k$ variables over $m$ equations in $\mathbb{F}_q[X]$.
    \item $k$, the number of variables we want our specialised system of equations to have, $1 \leq k \leq n$.
    \item $d$, the desired degree of the system of equations after we specialise the last $n - k$ variables, $1 \leq d < D$.
\end{enumerate}
The total degree in the first $k$ variables of a polynomial $p$ is labelled as $deg_kp$.

If we wish for our system of equations to reduce down to a linear system, then we set $d = 1$.
However, to extend this to larger values of $d$, we must construct new equations with $deg_kp \leq d$.
According to \cite{Vitse:2017cb}, this allows us to select smaller values of $D$ since we do not need to produce a Macaulay Matrix with a lot of polynomials in order to `break them down' to a  system of degree $d$.
This is desirable since $D$ must be large enough for any reduced equations to exist but also small enough to make the Macaulay Matrix manageable.

As previously mentioned, selecting values for $D, d$ and $k$ is a non-trivial task.

\subsection{Description of the algorithm}
To better understand the algorithm, let us divide it into two main steps, the pre-processing and then the actual algorithm.
Furthermore, let us also assume we are working with a boolean system of polynomial equations.
The pre-processing goes as following,\begin{enumerate}
    \item Construct the Macaulay Matrix of degree $D$ of a polynomial system of equations $F$ with its columns sorted in reverse graded lex.
          Label this matrix as $Mac_D$
    \item Let $Mac^k_{D,d}(F)$ be a submatrix of $Mac_D$ whereby each row $u_{ij}f_i$ represents a polynomial with the property that $deg_ku_{ij} \geq d-1$.
    \item Let $M^k_{D,d}(F)$ be a submatrix of $Mac_{D,d}^k$, whereby each column $i$ represents the monomial $M_i$ whereby $deg_kM_i > d$.
\end{enumerate}
The actual algorithm does the following,\begin{enumerate}
    \item Construct the cokernel of $M^k_{D,d}$ with elements $v_1,\hdots,v_r$, whereby $r$ is the number of elements in the cokernel.
    \item Compute new polynomials $p_i = v_i Mac^k_{D,d}(F)$.
          This forms a system of polynomial equations, $P = {p_1,\hdots,p_r}$, whereby they have a total degree at most $D$ and at most $d$ in $x_1,\hdots,x_k$.
    \item For all $a = \{a_{k+1},\hdots,a_n\} \in \mathbb{F}^{n-k}_2$,          \begin{enumerate}
              \item Partially evaluate the last $n-k$ variables of $F$ at $a$.
                    Let $F^{*}$ represent this new system.
              \item Construct a new Macaulay Matrix of degree $d$ of $F^{*}$.
                    Now, let $Mac_d(F^{*})$ represent this new matrix.
              \item Partially evaluate the last $n-k$ variables of polynomial system $P$ at $a$.
                    Let $P^{*}$ represent this new system as a coefficient matrix.
              \item Append $Mac_d(F^{*})$ to $P^{*}$.
                    Let $PM^{*}$ represent this new polynomial system of equations.
              \item Check if this system is consistent using dense linear algebra.
                    If it is, extract variables $x_1\hdots x_k$ and test the solution.
                    Output any valid solutions.
          \end{enumerate}
\end{enumerate}

According to \cite{Vitse:2017cb}, the number of equations of $P$ must be at least the number of monomials in $k$ variables of degree $d$, which is ${k + d - 1 \choose d}$ for $\mathbb{F}_{q>2}$ and $\sum_{d' = 0}^d{k \choose d'}$ for $\mathbb{F}_2$.
Thus, for $d = 1$ we need to simply check whether $|P| > k$.
This is to ensure enough independent relations to finally solve the system.

Note how we are using field equations for polynomial systems in $\mathbb{F}_2$.
This is equivalent of working within a quotient ring of the form ${\mathbb{F}_2}_/{\braket{x_1^2,\, \hdots ,\, x_n^2}}$ \cite{Bardet:2005ab}.
However, multiplication in quotient rings involves reducing the polynomials by the field equations to ensure that they are in the quotient ring.
This means that including field equations for large fields may not be computationally feasible.

\subsection{Understanding various matrices}
\subsubsection{First submatix}
By constructing $Mac^k_{D,d}(F)$, we obtain polynomials of the form $u_{i.j} \cdot f_i$ whereby the total degree of $u_{i.j}$ in the first $k$ variables is at least $d-1$.
For example, for $\mathbb{F}_2$, consider $n = 5$, $D = 4$, $d = 2$ and $k = 3$.
That means that the first $k$ variables are $x_1, x_2, x_3$ and the last $n-k$ are $x_4$ and $x_5$.
Let $f = x_1x_2 + 1$.
Consider the following row in $Mac_D$,

\begin{equation*}
    x_4x_5\cdot f = x_1x_2x_4x_5 + x_4x_5
\end{equation*}

Clearly, since $Mac_{D,d}^k$ only contains rows whereby the multiplier has at least total degree $d - 1 = 1$ in the first $k$ variables, we would not include this row.
This is because upon specialisation, such as $x_4 = x_5 = 1$, we would obtain $1\cdot f = x_1x_2 + 1$, which is simply $f$.
If we set $x_4 = x_3 = 0$ or any variation whereby at least one of the variables is $0$, we would obtain, $0\cdot f = 0$.

Hence, if we include these rows, we would simply obtain $0$ or our initial $f$.
None of these add any new information to our system because they will result in a linearly dependent row and thus, produce the trivial solution $0 = 0$.

\subsubsection{Cokernel}
Since multiplying a matrix by the elements of the cokernel is equal to $0$, multiplying the elements of the cokernel of $M^k_{D,d}(F)$ by $Mac^k_{D,d}(F)$, we simply `remove' all monomials in $Mac^k_{D,d}(F)$ that do not have a degree at most $d$ in the first $k$ variables as we remove columns whereby their total degree in the first $k$ variables is greater than $d$.
This will allow us to achieve a system of degree at most $d$ after specialising the last $n-k$ variables.

All of this boils down to the fact that we want to obtain an equivalent system of $F$ that has a degree at most $D$ and a degree of at most $d$ in the first $k$ variables.
As mentioned before, we want this because after we specialise the last $n-k$ variables, we obtain a smaller system with a total degree of at most $d$.

\subsubsection{Appending our initial equations}
For $d = 1$, $Mac_{D,d}^k(F) = Mac_D(F)$, which means that we have selected all of the equations to be used when constructing $P$.
The reason we create $Mac_d(F^*)$ is to include more equations since it reduces the amount of consistent systems that are obtained and therefore, you have less systems to test whether they are also consistent with $F$.
This may seem like a disadvantage but the consistent systems we have avoided would also not be consistent with $F$ and hence, we evaluate less systems that are bound to be incorrect Clearly, when $d = 1$, we are not producing any new information since all of the rows in $Mac_d(F^{*})$ would be linearly dependent with $P$.
Henceforth, we consider $Mac_d(F^*)$ to be empty when $d = 1$.


\subsection{Columns of second submatrix}
As we will see in Section \ref{sec:compest}, we need to have an equation that outputs the number of columns of $M_{D,d}^k$ to construct the complexity estimate of the Crossbred algorithm.
Firstly, let us establish that the number of monomials in $n$ variables from degree 0 up to, and including, $d$ is given by,\begin{equation}
    \label{eqn:monk}
    \mathcal{M}_{n,d} = {{n + d} \choose d} = \sum_{d'=0}^d {{n + d' - 1} \choose d'}
\end{equation}
However, in $\mathbb{F}_2$, we are working in a quotient field with field equations, so we adapt the above definition for boolean $\mathbb{F}_2$,\begin{equation}
    \label{eqn:mon2}
    \mathcal{M}_{n,d} = \sum_{d'=0}^d {{n} \choose d'}
\end{equation}
Since the columns of the Macaulay Matrix index monomials in $n$ variables from degree 0 to $d$, we can use the above equations to calculate its number of columns.
Let us continue with this example and let us construct $M_{D,d}^k$, which requires us to get rid of any monomials in the columns of $Mac_{D,d}^k$ whereby their degree in the first $k$ variables is smaller or equal to $d$.
If we use $d = 1$ and $k = 2$, all monomials that have a degree lesser or equal to $d$ in the first $k$ variables are removed.
In this case, we have $x_1x_2$.

However, we would also include, for example, $x_1x_2$ multiplied by any monomial that is comprised of the last $n-k$ variables such as $x_3$, such that the result is $x_1x_2x_3$.
This multiplication would have to result in a monomial whose total degree is at most $D$, hence, if we let $d_k$ represent our initial monomial's degree in the first $k$ variables (in this case, the initial monomial is $x_1x_2$), the degree of the monomial with variables from the last $n-k$ variables must be at most $D-d_k$.
Henceforth, this is the same as saying that we also want all monomials comprised of the last $n-k$ variables of degree 0 till $D-d_k$.
This leads us to the following equation for the number of columns for $\mathbb{F}_{q>2}$,\begin{equation} \label{eqn:mkfq}
    n_{cols}(M_{D,d}^k) =  \sum_{d_k = d + 1}^D \sum_{d' = 0}^{D - d_k} {k + d_k - 1 \choose d_k} {n-k + d' -1 \choose d'}
\end{equation}
In $\mathbb{F}_2$ \cite{Vitse:2017cb}, the number of columns would be,\begin{equation} \label{eqn:mkf2}
    n_{cols}(M_{D,d}^k) = \sum_{d_k = d + 1}^D \sum_{d' = 0}^{D - d_k} {k \choose d_k} {n-k \choose d'}
\end{equation}


\section{Selecting parameters}
\subsection{Incorrect parameter selection}
Let us show how incorrectly choosing parameters can lead to the algorithm not functioning.
Consider the columns of $M_{D,d}^k$, whose columns contain monomials whereby their total degree in the first $k$ variables is larger than $d$.
If we have $k = 2$, the first $k$ variables are $x_1$ and $x_2$.
In $\mathbb{F}_2$, the maximum degree obtainable with 2 variables is with the monomial $x_1x_2$ as we cannot have squares.
Henceforth, $M^k_{D,d}$ is empty as it is impossible for any monomials in the first $k$ variables to be larger than 2.  Since it is an empty matrix, it does not have a cokernel.

Therefore, if the number of degree $d$ monomials over $k$ is lesser than $d$, the algorithm will not function properly.
For $\mathbb{F}_2$, this is equivalent of saying $\sum_{d'= 0}^d {k \choose d'} < d$ and for $\mathbb{F}_{q>2}$, ${{k + d - 1} \choose d} < d$.

\subsection{Admissibility of parameters}
For $d > 1$, to determine the admissibility of the parameters, \cite{Vitse:2017cb} derived a bivariate generating series.
Firstly, let us define,\begin{equation}
    \label{eqn:skd}
    S^k_{D,d} = \frac{(1+X)^{n-k}}{(1-X)(1-Y)} \left(\frac{(1+ XY)^k}{(1+X^2Y^2)^m}\right. - \left.\frac{(1+X)^k}{(1+X^2)^m} \right)
\end{equation}
The coefficient of $X^DY^d$ of $S^k_{D,d}$ represents the number of new independent polynomials after the reduction of $Mac_{D,d}^k$, which is equivalent to corank of $M^k_{D,d}$.
The reason for this will be explained in the next section.

If the coefficient of $X^DY^d$ is non-negative in the following equation, then the parameters $(D,\,d,\,k)$ for the algorithm are admissible,\begin{equation}
    \label{eqn:bivariate}
    A^k_{D,d} = S^k_{D,d} - \frac{(1+Y)^k}{(1-X)(1-Y)(1+Y^2)^m}
\end{equation}
The reason why will be explained further down this section.

For example, for a system with 3 variables and 4 equations, let $k = 2$.
The admissibility series will produce,\begin{equation}
    2X - X^2 + 10X^3 + 3X^2Y + \hdots + 8X^3Y^2 + \hdots
\end{equation}

Since the coefficient of $X^3Y^2$ is non-negative (\ie{} 8), $D = 3$, $d = 2$ and $k = 2$ are admissible parameters for this polynomial system.

\section{Understanding and deriving the generating series}

\subsection{Informal analysis of the generating series}
Let us now break down the various parts of the bivariate generating series.
By expanding out the multiplication that occurs in $S^k_{D,d}$ and ignoring the $\frac{1}{1-X}$ and $\frac{1}{1-Y}$ parts as they will be explained later, we obtain,\begin{equation}
    \label{eqn:bivariate2}
    \frac{(1+XY)^k(1+X)^{n-k}}{(1+X^2Y^2)^m} - \frac{(1+X)^n}{(1+X^2)^m}
\end{equation}
Informally, the leftmost term represents the formal power series of the corank of $M_{D,d}^k$ and the rightmost term represents the formal power series of the corank of $Mac_D$.
This allows for us to obtain the corank of these matrices for various values of parameters.
We have already discussed how the evaluation of a Hilbert Function $HF'_I(d)$ is by definition the corank of a Macaulay Matrix of degree $d$ in Section \ref{sec:relationdegreg}.
Proof of this will be provided in the following section.

As stated before, finding the dimension of the left kernel of $M^k_{D,d}$ will tell us the number of polynomials produced in $P$.
Furthermore, we need to know how many of these polynomials are new in relation to our initial system, $F$, as we are going to be including our initial equations alongside $P$ since $d > 1$ .
This is why we subtract the Hilbert Series of our initial Macaulay Matrix of degree $D$, which is the rightmost term of Equation~\ref{eqn:bivariate2}.

Consider the subtraction that occurs on the left hand side of the admissibility series (once again, omitting the $\frac{1}{1-X}$ and $\frac{1}{1-Y}$ parts),\begin{equation}
    A^k_{D,d} = S^k_{D,d} - \frac{(1+Y)^k}{(1+Y^2)^m}
\end{equation}
This subtracts the corank of the new polynomials after evaluation, which removes the number of polynomials that reduce to $0$ after evaluation.
Let us refer to this term of pure $Y$ as $S^k_0$.

Let $S^{'k}_{D,d}$ and $S^{'k}_0$ refer to  $S^{k}_{D,d}$ and $S^{k}_0$ without the $\frac{1}{1-X}$ and $\frac{1}{1-Y}$ parts, respectively.
Hence,\begin{equation*}
    \begin{aligned}
        S_{D,d}^{'k}   & = {(1+X)^{n-k}} \left(\frac{(1+ XY)^k}{(1+X^2Y^2)^m} - \frac{(1+X)^k}{(1+X^2)^m} \right) \\
        \quad S^{'k}_0 & =  \frac{(1+Y)^k}{(1+Y^2)^m}
    \end{aligned}
\end{equation*}
The use of $\frac{1}{1-X}$ and $\frac{1}{1-Y}$ is to copy $S^{'k}_{D,d}$ and $S^{'k}_0$ to all degrees of $X$ and $Y$.
What this means is that since the expansion of $\frac{1}{1-X} = 1 + X + X^2 + \hdots$, we obtain,\begin{equation*}
    \begin{aligned}
        \frac{(S^k_{D,d}-S^k_0) }{(1-X)(1-Y)} = X^0Y^0(S^k_{D,d}-S^k_0) + X^1Y^1(S^k_{D,d}-S^k_0) + X^2Y^2(S^k_{D,d}-S^k_0) \hdots
    \end{aligned}
\end{equation*}
This means that all possible combinations of $X^DY^d$ are included in the series.
Hence, if the coefficient of $X^DY^d$ is a non-negative number, it means that the set of parameters will produce a non-negative amount of new polynomials, which is why we consider the parameters admissible.
Note how we can produce 0 new polynomials, but this is allowed since we append our initial system.

In conclusion, the proof that $\frac{(1+Y)^k}{(1+Y^2)^m}$ is the formal power series for the corank the polynomials that reduce to 0 and that $\frac{(1+X)^n}{(1+X^2)^m}$ is the formal power series for the corank of the Macaulay Matrix will not be presented as they follow trivially from \cite[p.65-66]{Bardet:2004ec}.
However, to complete the proof, we must demonstrate a non-trivial proof of the formal power series of $M^k_{D,d}$.

\subsection{Deriving the admissibility series for boolean finite fields}

\subsubsection{Number of monomials in second submatrix}
Let us now derive the admissibility series from the Hilbert Function as to provide clarity and mathematical proof of its correctness.

\begin{proposition}
    The number of number of monomials with degree less than or equal to $D$ and to $d$ in the first $k$ variables can be obtained by the following generating series in a boolean finite field,
    \begin{equation}
        \mathcal{M}_{\leq D,\leq d, m} = \frac{(1+X)^{k}(1+XY)^{n-k}}{(1-X)(1-Y)}
    \end{equation}
\end{proposition}

\begin{proof}
    From Equation \ref{eqn:mkf2}, we get the equation for the number columns of $M^k_{D,d}$,
    \begin{equation}
        \mathcal{M}(M^k_{D,d}) =  \sum_{d_k = d + 1}^D \sum_{d' = 0}^{D - d_k} {k \choose d_k} {n-k \choose d'}
    \end{equation}
    From this, we can deduce the number of monomials with degree less than or equal to $D$ and to $d$ in the first $k$ variables,
    \begin{equation}
        \mathcal{M}_{\leq D,\leq d, m} =  \sum_{d_k = 0}^d \sum_{d' = 0}^{D - d_k} {k \choose d_k} {n-k \choose d'}
    \end{equation}
    and hence, given that $\sum_{d \geq 0}{n \choose d} z^d = (1 + z)^n$,  we obtain we want to copy this to all degrees of $X$ and $Y$,
    \begin{equation}
        \mathcal{M}_{\leq D,\leq d, m} = \frac{(1+XY)^k(1+X)^{n-k}}{(1-X)(1-Y)}
    \end{equation}
\end{proof}

\subsubsection{Hilbert Function and Hilbert Series for second submatrix}

Let us now introduce a slightly altered definition of the Hilbert Function to allow us to derive the generating series for  $M^k_{D,d}$.
For the readability of the following theorem and proof, we will denote $M^k_{D,d}$ and $M^{f,k}_{D,d,m}$ whereby $m$ is the number of equations in our initial system, $f$.
If we use a subset of the first $v \leq m$ equations of our initial system, we write $M^{f,k}_{D,d,v}$.

\begin{definition}
    (Hilbert Function for ideal generated by equations in $M^{f,k}_{D,d,m}$)
    Given $m$ equations in $\mathbb{F}_2[X]$, an ideal $I$ generated by said equations (i.e $I = \braket{f = f_1,\hdots f_m}$), we define the Hilbert Function for $M^{f,k}_{D,d,m-1}$ as,
    \begin{equation}
        HF'_{I,m}(D,d,k) = \dim(\mathbb{F}_2[X]_{D,d,k}) - \dim(I_{D,d,k})
    \end{equation}
    whereby
    \begin{itemize}
        \item $\mathbb{F}_2[X]_{D,d,k}$ is the set of all monomials in $\mathbb{F}_2[X]$ of degree at least $d+1$ in its first $k$ variables.
        \item $I_{D,d,k}$ is the set of all $f \in \mathbb{F}_2[X]$ in the form $mg$, whereby $m$ is a monomial of degree at least $d+1$ in its first $k$ variables and $f$ has total degree of at most $D + d - 1 $.
    \end{itemize}
\end{definition}

We can calculate this altered Hilbert Function from the standard Hilbert Function by simply removing the polynomials of form $uf$ for a monomial $u$ and polynomial $f$ whereby $deg_k u \geq d-1$ and then all monomials in $uf$ whereby $deg_k M_i > d$.

\begin{theorem} \label{theorem:hf-weird}
    Given $m$ homogeneous linearly independent equations in $\mathbb{F}_2[X]$, $f_1\hdots f_m$, then we can recursively define the Hilbert Function for $M^{f,k}_{D,d,m}$ as,
    \begin{equation} \label{eqn:hf-relation}
        HF'_{I,m}(D,d,k) = HF'_{I,m-1}(D,d,k) -  HF'_{I,m}(D-2,d,k,m)
    \end{equation}
\end{theorem}

\begin{proof}
    This proof closely follows the proof of \cite[Lemma 3.3.6]{Bardet:2004ec}, with the exception that we now use the altered definition of the Hilbert Function for the ideal generated by the equations in  $M^{f,k}_{D,d,m}$.

    To do this, we start with $Mac^{m-1}_D$ simply add equations with the form $u \cdot f_m$.
    However, we will need to remove some equations due to the General Criteria and the Frobenius Criteria, which in this case is equal to the number of columns in $Mac^{m}_{D-2}$.
    However, as we are want to use the submatrix of $Mac^{m}_{D}$ and $Mac^{m}_{D-2}$, we simply remove all the polynomials of form $uf$ for a monomial $u$ and polynomial $f$ whereby $deg_k u \geq d-1$ and then all monomials in $uf$ whereby $deg_k M_i > d$.
    If we then remove these polynomials and monomials and denote the number of rows of $M^{f,k}_{D,d,m}$ as $U_{D,d,m}$, then we have,
    \begin{equation}
        U_{D,d,m} = U_{D,d,m-1} + \mathcal{M}_{\leq D-2,\leq d, m} - U_{D-2,d,m}
    \end{equation}
    with $ U_{D,d,0} = 0$ or $D$ is smaller than the smallest total degree of equations $f_1,\hdots,f_m$.
    We can rewrite this as $U_{D,d,m} - U_{D,d,m-1} = \mathcal{M}_{\leq D-2,\leq d, m} - U_{D-2,d,m}$.
    Since  $HF'_{I,m}(D,d,k) = \mathcal{M}_{\leq D,\leq d, m} - U_{D,d,m}$, we obtain $HF'_{I,m}(D,d,k) -  HF'_{I,m-1}(D-2,d,k) = U_{D,d,m-1} - U_{D,d,m}$.
\end{proof}

\begin{theorem} \label{theorem:admis}
    Given $m$ linearly independent equations in $\mathbb{F}_2[X]$, $f_1\hdots f_m$ with degrees $d_i$ for $i \in \{0,\hdots,m\}$, then we can recursively define the Hilbert Series for the ideal generated by the equations in $M^{f,k}_{D,d,m}$ as,
    \begin{equation}
        S_{m,n,D,d,k}(X,Y) = \sum_{D,d \geq 0} HF'_{I,m}(D,d,k) X^D Y^d = \frac{(1+XY)^k(1+X)^{n-k}}{(1+X^2Y^2)^m(1-X)(1-Y)}
    \end{equation}
\end{theorem}

\begin{proof}
    Once again, this closely follows the proof of \cite[Proposition 3.3.7]{Bardet:2004ec}.
    Consider the following generating series adapted from the recursive definition of the Hilbert Function in Theorem \ref{eqn:hf-relation}, we get,
    \begin{equation}
        \begin{split}
            \sum_{D,d \geq 0} & HF'_{I,m}(D,d,k) X^D Y^d
            \\ &= \sum_{D,d \geq 0} HF'_{I,m}(D,d,k) X^D Y^d - \sum_{D,d \geq 0} HF'_{I,m-1}(D-2,d,k) X^D Y^d
            \\ &= \sum_{D,d \geq 0} HF'_{I,m-1}(D,d,k) X^D Y^d - X^{2}Y^{2} \sum_{D,d \geq 0} HF'_{I,m}(D,d,k) X^D Y^d
        \end{split}
    \end{equation}
    and hence,
    \begin{equation} \label{eqn:almost-last-step}
        \begin{split}
            \sum_{D,d \geq 0} HF'_{I,m}(D,d,k) X^D Y^d &= \frac{1}{1+X^2Y^2} \sum_{D,d \geq 0} HF'_{I,m-1}(D,d,k) X^D Y^d
            \\ &= \frac{1}{(1+X^2Y^2)^{m-1}} \sum_{D,d \geq 0} HF'_{I,1}(D,d,k) X^D Y^d
        \end{split}
    \end{equation}
    and since $HF'_{I,1}(D,d,k) = \mathcal{M}_{\leq D,\leq d, m} - HF'_{I,1}(D-2,d,k)$, we can deduce that,
    \begin{equation}
        \begin{split}
            \sum_{D,d \geq 0} HF'_{I,1}(D,d,k) X^D Y^d &= \frac{1}{1+X^2Y^2} \sum_{D,d \geq 0}\mathcal{M}_{\leq D,\leq d, 1} X^D Y^d
            \\ &= \frac{(1+X)^{n-k}(1+XY)^{k}}{(1+X^2Y^2) (1-X)(1-Y)}
        \end{split}
    \end{equation}
    and plugging this into Equation \ref{eqn:almost-last-step}, we obtain,
    \begin{equation}
        S_{m,n,D,d,k}(X,Y) = \sum_{D,d \geq 0} HF'_{I,m}(D,d,k) X^D Y^d = \frac{(1+X)^{n-k}(1+XY)^{k}}{(1+X^2Y^2)^m (1-X)(1-Y)}
    \end{equation}
\end{proof}


\subsection{Admissibility series for non-boolean finite fields}
Recall the definition of the Hilbert Series for homogenous semi-regular system of $m$ equations in $\mathbb{F}_{q>2}$,
\begin{equation*}
    S_{m,n} = \frac{(1-t^{q})^m}{(1-t)^n}
\end{equation*}
The only difference for deriving the bivariate generating series for $\mathbb{F}_{q > 2}[X]$ and Theorem \ref{theorem:admis} is that we do not use the Frobenius Criteria as squares do not reduce to 0 and the number of monomials in the columns of $M^{k}_{D,d}$ is different.

In terms of the number of monomials in the in the columns of $M^{k}_{D,d}$ is,
\begin{equation*}
    \sum_{d_k = d + 1}^D \sum_{d' = 0}^{D - d_k} {k + d_k - 1 \choose d_k} {n-k + d' -1 \choose d'}
\end{equation*}
and hence, due to $\sum_{d \geq 0}{n + d - 1 \choose d} z^d = \frac{1}{(1 - z)^n}$, the generating series for the number monomials with degree less than or equal to $D$ and to $d$ in the first $k$ variables is,
\begin{equation*}
    \mathcal{M}_{\leq D,\leq d, m} = \frac{1}{(1-X)^{n-k}(1-XY)^{k}(1-X)(1-Y)}
\end{equation*}
Furthermore, due to the fact we do not use the Frobenius Criteria, we obtain,
\begin{equation}
    U_{D,d,m} = U_{D,d,m-1} + \mathcal{M}_{\leq D-q,\leq d, m} - U_{D-q,d,m-1}
\end{equation}
and following a similar logic for the proof of Theorem \ref{theorem:hf-weird}, we get the recursive relationship,
\begin{equation}
    HF'_{I,m}(D,d,k) = HF'_{I,m-1}(D,d,k) -  HF'_{I,m-1}(D-q,d,k)
\end{equation}
From this, we can construct the Hilbert Series following the exact same logic as before.

\begin{theorem}
    Given $m$ linearly independent equations in $\mathbb{F}_{q > 2}[X]$, $f_1\hdots f_m$ with degrees $d_i$ for $i \in \{0,\hdots,m\}$, then we can recursively define the Hilbert Series for the ideal generated by the equations in $M^{f,k}_{D,d,m}$ as,
    \begin{equation}
        S_{m,n,D,d,k}(X,Y) = \sum_{D,d \geq 0} HF'_{I,m}(D,d,k) X^D Y^d = \frac{(1-X^qY^q)^m}{(1-X)^{n-k}(1-XY)^{k}(1-X)(1-Y)}
    \end{equation}
\end{theorem}

Therefore, if we follow the same idea (omitting the Frobenius Criteria) for Equation~\ref{eqn:bivariate}, the admissibility series for homogeneous system of equations in $\mathbb{F}_{q>2}$ will be,
\begin{equation}
    A^k_{D,d,q} = \frac{1}{(1-X)(1-Y)} \left(\frac{(1-X^{q}Y^{q})^m}{(1 - XY)^k(1 - X)^{n-k}}\right. - \left.\frac{(1-X^{q})^m}{(1-X)^n} - \frac{(1-Y^{q})^m}{(1-Y)^k}\right)
\end{equation}
In $\mathbb{F}_{q>2}$, the parameters $(D, d, k)$ are admissible if the coefficient of $X^DY^d$ in $A^k_{D,d,q}$ is non-negative.

\section{Complexity of the Crossbred algorithm}

\subsection{Complexity estimate}
\label{sec:compest}
The complexity of the algorithm for any homogeneous polynomial system in $\mathbb{F}_q$ has the following form,\begin{equation*}
    \mathcal{C}_{cross_q} = \mathcal{O}(\texttt{kernel}(M_{D,d}^k)) + q^{n-k}\cdot\mathcal{O}((\texttt{solving}(P^*\cup Mac_d(F^*))))
\end{equation*}
Recall that $P^*$ is the system $P$ upon evaluation of the last $n-k$ variables.
Block Wiedemann or Lancz\"os algorithms can be used to calculate the cokernel of a sparse matrix.
The complexity of finding cokernel vectors of a sparse matrix is $\tilde{\mathcal{O}}(n_{cols}^2)$, whereby $n_{cols}$ the number of columns in our matrix \cite{Giesbrecht:1998cs}.
We have already established how to calculate the number of columns of the cokernel $M_{D,d}^k$ in the previous section using either Equation \ref{eqn:mkfq} or Equation \ref{eqn:mkf2}.

We can also use the block Wiedemann or Lancz\"os to probabilistically test the consistency and find a small number of solutions (if any) of a set of polynomials \cite{Vitse:2017cb}, which in our case, is $P^*\cup Mac_d(F^*)$ for $d > 1$ and just $P$ for $d = 1$.
The complexity of doing so is $\tilde{\mathcal{O}}(n_{cols}^\omega)$ whereby $\omega$ is the exponent of matrix multiplication.
Let us set $\omega = 2$.\footnote{In reality, at the time of writing, the smallest practical value of $\omega$ is 2.807, but we consider the worse-case scenario.}

The number of columns of both $P^*\cup Mac_d(F^*)$ and $P$ is equal to the number of monomials in $k$ variables from degrees $0$ to $d$.
We have also already established how to calculate this in the previous section using either Equation \ref{eqn:monk} or Equation \ref{eqn:mon2}.
Therefore, we can write the complexity estimate for the Crossbred Algorithm as,
\begin{equation}
    \mathcal{C}_{c_{q>2}} = \tilde{\mathcal{O}}\left(\left(\sum_{d_k = d + 1}^D \sum_{d' = 0}^{D - d_k} {k + d_k - 1 \choose d_k}{n-k + d' -1 \choose d'}\right)^2\right) + q^{n-k}\cdot\tilde{\mathcal{O}} \left({k + d - 1\choose d}^2\right)
\end{equation}
And in $\mathbb{F}_2$,
\begin{equation}
    \mathcal{C}_{c{q=2}} = \tilde{\mathcal{O}}\left(\left(\sum_{d_k = d + 1}^D \sum_{d' = 0}^{D - d_k} {k \choose d_k} {n-k \choose d'}\right)^2\right) + 2^{n-k}\cdot\tilde{\mathcal{O}}\left(\left(\sum_{i=0}^d {k \choose i}\right)^2\right)
\end{equation}

For the rest of the manuscript, when discussing cases where the value of $q$ could be $\geq 2$ we refer to the complexity of the Crossbred algorithm as $\mathcal{C}_{cross_q}$, which could either be  $\mathcal{C}_{cross_{q=2}}$ or  $\mathcal{C}_{cross_{q>2}}$.


\subsection{Comparison with MQDSS's estimate}
The supporting documentation for a NIST Post Quantum Cryptography Round 2 candidate based on multivariate cryptography, MQDSS by \cite{NISTPQC-R2:MQDSS19}, provides a complexity for the Crossbred algorithm for $\mathbb{F}_{q > 2}$, which has a very similar form,\begin{equation}
    C_{cross_{q>2}} = \mathcal{O}\left({n+D-1 \choose D}^2\right) + \log(n - k) \cdot q^{n-k}\cdot{\mathcal{O}} \left({k + d - 1\choose d}^\omega\right)
\end{equation}

Note that Samardijska et al.\  included $\log(n - k)$.
This is because $\log(n - k)$ is the amount of field operations necessary to specialise $n - k$ variables \cite{NISTPQC-R2:MQDSS19}.

The $\mathcal{O}\left({n+D-1 \choose D}^2\right)$ part represents the complexity of finding kernel vectors in $M^k_{D,d}$ using, for example, the block Wiedemann algorithm.
The only difference between the complexity estimate of this step in relation to the estimate provided in this work is the number of columns of $M^k_{D,d}$.
Samardijska et al.\  assumes that $M^k_{D,d}$ has ${n+D-1 \choose D}$ columns, which is equivalent to the number of monomials in $n$ variables of degree up to $D$.
Clearly, $M^k_{D,d}$ will not have the same number of columns as the the initial Macaulay Matrix, making this estimate inaccurate.
However, this may be interpreted as an upper bound.

The last ${\mathcal{O}} \left({k + d - 1\choose d}^\omega\right)$ represents solving an overdetermined system of multivariate polynomials with $k$ variables of degree $d$ via computing the Gaussian Elimination of a large matrix, which represents solving the system.
This is the same in our complexity estimate.
Hence, the only real difference between the estimate provided in this manuscript is that the estimate presented by Samardijska et al.\  may be interpreted as an upper bound.


