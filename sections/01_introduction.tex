%!TEX root = main.tex
\section{Introduction}
Currently, public key cryptosystems such as RSA rely on the difficulty of factoring a large number into two prime factors.
Furthermore, with the development of Shor's Algorithm for quantum computers, the integer factorisation and discrete logarithm problems are solvable in polynomial time with a quantum computer of sufficient size, rather than exponential time with classical algorithms.
Henceforth, when quantum computers become a feasible tool for computation, cryptosystems that rely on these aforementioned problems will be considered insecure.

Research into cryptography that is secure against an attack by a quantum computer (post-quantum cryptography) has heavily contributed to the continued development of multivariate cryptography.
Its security relies on the hardness of solving a set of multivariate polynomial equations.
There are no known polynomial time algorithms that solve such a problem for quantum computers and solving a set of random multivariate polynomials is an NP-complete problem \cite[p.194]{Ding:2009a}.
Due to this, it is a fitting candidate for replacing current public-key cryptography.

However, it is believed that there may exist more efficient classical algorithms for solving multivariate polynomial system of equations than what currently exists.
This is why it is important to thoroughly study and understand new algorithms, such as the Joux--Vitse Crossbred algorithm.
The Crossbred algorithm's aim is to efficiently solve a system of semi-regular multivariate polynomials equations.

\subsection{Introduction to the Joux--Vitse Crossbred Algorithm}
The Crossbred algorithm was created by \cite{Vitse:2017cb}.
Their purpose was to produce a scalable algorithm that solves random systems of multivariate polynomial equations by combining ideas from the BooleanSolve algorithm \cite{Bardet:2013bs} and the Kipnis-Patarin-Goubin algorithm \cite{Kipnis:1999us}, hence it being called `Crossbred'.
Specifically, given a multivariate system of polynomials equations, it will produce an equivalent smaller system which is more easily solved.
Joux and Vitse only worked with systems in boolean $\mathbb{F}_2$, but stated that this algorithm also works for small finite fields $\mathbb{F}_{q>2}$.
In addition, there are details to the algorithm which are not explicitly explained, such as the reasoning behind constructing some submatrices.
This manuscript will fully explain the algorithm.

Some issues were found with the Joux--Vitse Crossbred algorithm, namely that upon performing asymptotic analysis, Joux and Vitse determined that it ultimately does not provide an asymptotic improvement in relation to BooleanSolve and FXL.
Due to this disappointing result and restrictive page limits, they did not expand on this analysis, nor did they present a complexity estimate of their algorithm.
Thus, there is room to investigate their algorithm's complexity for both boolean $\mathbb{F}_2$ and $\mathbb{F}_{q>2}$ and investigate if it provides any asymptotic improvement in relation to other algorithms, such as the similar state-of-the-art algorithm, Hybrid-$F_5$, and the Fast Exhaustive Search (FES).

In the supporting documentation of a NIST Post-Quantum Cryptography Round 2 candidate, MQDSS, Samardijska et al.\  provided a rough complexity estimate for the Crossbred algorithm, but they did not provide a clear explanation on how the estimate was obtained \cite{NISTPQC-R2:MQDSS19}.
This allows us to see if their complexity estimate matches with the one derived in this work and compare any differences.
In the end, it is determined that their estimate can be viewed as an upper-bound.

The Crossbred algorithm's course depends on three parameters.
Selecting these parameters is essential as an `incorrect' selection will cause the algorithm to either not function (\eg{, producing empty submatrices and the algorithm cannot continue its execution}) or not produce to any solutions (if they exist).
If the algorithm is functional (\eg{, does not produce any empty submatrices}) and outputs a solution (if any exist), then the parameters are called admissible.  Henceforth, selecting functional parameters is a non-trivial task.
To address this issue, Joux and Vitse presented a bivariate generating series which determines if the parameters provided to the algorithm are admissible in $\mathbb{F}_2$ before execution.
However, its derivation or why the series works is not explained.
Hence, there is room to explain its derivation, why it works and present a bivariate generating series for $\mathbb{F}_{q>2}$.
