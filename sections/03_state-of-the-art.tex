%!TEX root = main.tex
\section{State-of-the-Art for Solving Multivariate Polynomials}
\subsection{Exhaustive Search}
This is the most basic way of solving a system of polynomial equations. If $q$ is a power of a prime and $n$ is the number of variables, we iterate over $\mathbb{F}^n_q$ and test if they produce a valid solution for our polynomials. Since there are $q^n$ values to test, the complexity of this algorithm is $\mathcal{O}(mq^n)$ for $m$ polynomials.

In $\mathbb{F}_2$, Fast Exhaustive Search (FES) was proposed by \cite{CHES:BCCCNS10} and it efficiently enumerates over the search space of $2^n$ such that the complexity of exhaustive search is (hopefully) less than $\mathcal{O}(mq^n)$. In $\mathbb{F}_2$, the complexity of FES is $\mathcal{O}(\log_2(n)\cdot 2^n)$ and is independent of the number of polynomials, $m$. We can expand FES for larger fields using $q-$ary Gray codes codes with a complexity of $\log_q(n)q^n$.

\subsection{Hybrid-F5}
$F_4$ was created by \cite{Faugere:1999af4} and it outputs a Gr\"obner basis for a given set of polynomials. An improved version called $F_5$ was later developed by \cite{Faugere:2002f5}.

Hybrid-$F_5$ \cite{Faugere:2008hf5} combines exhaustive search and $F_5$ by specialising $k$ variables and running the $F_5$ algorithm over the remaining $n-k$ variables. The most costly part in the complexity of $F_5$ is the row reduction of a matrix of size ${n + d_{reg} \choose d_{reg}}$ for $q > 2$ and ${n, d_{reg}}$ hence once can simplify its complexity estimate to:
\begin{align}
    \begin{aligned}
        C_{{F_5},q>2} & = \mathcal{O}\left({n + d_{reg} \choose d_{reg}}^\omega \right) \\
    \end{aligned}
\end{align}
whereby $2 \leq \omega \leq 3$ is the exponent of matrix multiplication. We set $\omega = 2$. By combining the above complexity estimate and exhaustive search, we obtain the complexity estimate for Hybrid-$F_5$ \cite{NISTPQC-R2:LUOV19}:
\begin{align}
    \begin{aligned}
        C_{{Hybrid},q>2} & = q^{k} \cdot \mathcal{O}\left({n - k - 1 + d_{reg}(n-k) \choose d_{reg}(n-k)}^2 \right) \\
    \end{aligned}
\end{align}
whereby $d_{reg}(n-k)$ is the degree of regularity of the system after evaluating $k$ variables and hence, having $n-k$ variables left.
For $q=2$, due to the square-free field, we obtain the complexity estimate
\begin{align}
    \begin{aligned}
        C_{{Hybrid},q=2} & = q^{k}\cdot\tilde{\mathcal{O}}\left(\left(\sum_{i=0}^{d_{reg}(n-k)} {n-k \choose i}\right)^2\right) \\
    \end{aligned}
\end{align}

\subsection{FXL/BooleanSolve}
The BooleanSolve algorithm was developed by \cite{Bardet:2013bs} before the Crossbred algorithm. This algorithm specialises the first $n - k$ variables in the polynomials by iterating it through $\mathbb{F}^{n-k}_2$. It then tests the consistency of the system via Macaulay Matrices of $d_{reg}(k)$, whereby $d_{reg}(k)$ is the degree of regularity of the system after specialisation. If this system is not consistent, iterate to the next value. Otherwise, exhaustive search is conducted over the first $k$ variables \cite{EPRINT:FHKKKP17}. The complexity of this algorithm is $\mathcal{O}(2^{0.841n})$ and a probabilistic variant called the Las Vegas variant has a conditional complexity of only $\mathcal{O}(2^{0.792n})$. Furthermore, a quantum version of the Las-Vegas variant was shown to only require the evaluation of $\mathcal{O}(2^{0.462n})$ quantum gates \cite{EPRINT:FHKKKP17}.

BooleanSolve can be seen as a specific instance of FXL \cite{Courtois:2000xl}, which does the same as BooleanSolve, with the exception that it supports finite fields larger than 2.
The complexity of FXL is \cite{ICISC:YanChe04}\cite{Bardet:2013bs},
\begin{equation}
    C_h = q^k \cdot \max \{\left ({n - k + d_0 \choose n - k}\right )^2,\,m\left ({n - k - q + d_0 \choose n - k }\right )\}
\end{equation} 
whereby $d_0$ is the first non-positive coefficient of the seroes expansion of $\frac{(1-t^q)^m}{(1-t)(1-t)^{n-k}}$.