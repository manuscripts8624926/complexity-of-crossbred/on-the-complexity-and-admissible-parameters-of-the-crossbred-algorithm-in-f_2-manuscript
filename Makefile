# You want latexmk to *always* run, because make does not have all the info.
# Also, include non-file targets in .PHONY so they are run regardless of any
# file of the given name existing.
.PHONY: submission all clean

all: submission

submission:
	latexmk -bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make -g main.tex

clean:
	latexmk -c main.tex
